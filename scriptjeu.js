window.onload = init;

const tailleMinCollection = 6;
let nbImages = tailleMinCollection;
let collection = "chiens";
let format = "webp";

function init() {
    console.log("init");
    let  numero = 0;
    genererPlateau(nbImages, collection, format);
    genererSelecteurCollection();
    genererSelecteurTaille();
}

function reinit(nouvelleTaille,nouvelleCollection, nouveauFormat) {
    console.log("réinit nouvelle collection");
    genererPlateau(nouvelleTaille, nouvelleCollection, nouveauFormat);
    //genererSelecteurTaille();
}

function definirTheme(event) {
    console.log("definir thème");
    var selectionTheme = event.currentTarget.value;
    var selectionFormat = definirFormat(selectionTheme);
    var selectionTaille = tailleMinCollection;
    reinit(selectionTaille,selectionTheme, selectionFormat);
}

function definirFormat(collection) {
    console.log("définir format :" + collection);
    if (collection == "alphabet-scrabble") {
        var format = "png";
    } else if (collection == "animaux" || collection == "animauxAnimes" || collection == "chiens") {
        var format = "webp";
    } else if (collection == "animauxdomestiques" || collection == "dinosaures" || collection == "dinosauresAvecNom") {
        var format = "jpg";
    }
    return format;
}

function definirTaille (event){
    console.log("définir Taille");
    var selectionTheme = document.getElementById('selecteurTheme').value;
    var selectionFormat = definirFormat(selectionTheme);
    var selectionTaille = event.currentTarget.value;
    reinit(selectionTaille,selectionTheme, selectionFormat);
}

// function definirTailleMax(collection) {
//     console.log("définir taille max");
//     var tailleMax = 0;
//     switch (collection) {
//         case "alphabet-scrabble":
//             tailleMax = 26;
//             break;
//         case "animaux":
//             tailleMax = 28;
//             break;
//         default:
//             tailleMax = 6;
//     }
//     return tailleMax;
// }



// FONCTION INITIALISATION TABLEAU INDEX

function initialiserIndex(nbImages) {
    let indexImages = new Array();
    for (let i = 0; i < nbImages; i++) {
        indexImages[i] = (i + 1);
        indexImages[(i +1 + nbImages)] = (i + 1);
    }
    return indexImages;
}

//FONCTION MELANGER TABLEAU INDEX

function melangerIndex(tab) {
    tab.sort((a, b) => Math.random() - 0.5);
    return tab;
}

//FONCTION GENERATION DUN CHEMIN VERS IMAGE

function genererChemin(collection, numero, format) {
    let cheminImage = 'ressources/' + collection + '/' + numero + '.' + format;
    console.log(cheminImage);
    return cheminImage;
}


//FONCTION GENERATION DU PLATEAU

function genererPlateau(nbImages, collection, format) {
    let tabIndexInitial = initialiserIndex(nbImages);
    console.log(tabIndexInitial);   
    let tabIndexMelange = melangerIndex(tabIndexInitial);
    console.log(tabIndexMelange);
    let index_images = tabIndexMelange;

    let mon_plateau = "";
    for (let i = 0; i < (nbImages*2); i++) {
        mon_plateau += "<div><img class='carre_interrogation' id='carre_test_" + (i + 1) + "' src='ressources/question.svg' alt='question'>";
        mon_plateau += "<img class='carre_img' id='img" + (i + 1) + "' src='" + genererChemin(collection, index_images[i], format) + "' alt='image'></div>";
    }

    console.log("Mon plateau :" + mon_plateau);
    document.getElementById('plateauGenere').innerHTML = mon_plateau;

    for (let i = 0; i < (nbImages*2); i++) {
        document.getElementById('carre_test_' + (i + 1)).addEventListener("click", montrerImage);
    }
}

var choix1;
var choix2;
var nb_de_clics = 0;
var nb_essais = 0;
var nb_paires_retournees = 0;

//FONCTION montrer Image du jeu

function montrerImage(event) {
    nb_de_clics++;
    console.log("nombre de clics :" + nb_de_clics);

    event.currentTarget.style.display = "none";
    //console.log("visibilité de la cible cliquée: " + event.currentTarget.style.display);

    let sibling = event.currentTarget.nextElementSibling;
    sibling.style.display = "block";
    //console.log(event.currentTarget.nextElementSibling);
    //console.log(event.currentTarget.nextElementSibling.style.display);
    if (nb_de_clics === 1) {
        choix1 = sibling;
        console.log("choix1 =" + choix1.getAttribute('src'));
    } else {
        choix2 = sibling;
        console.log("choix2 =" + choix2.getAttribute('src'));

        comparerImages(choix1, choix2);
        nb_de_clics = 0;
        nb_essais++;
        console.log("Nombre de coups : " + nb_essais);
        document.getElementById('score').innerHTML = "Nombre de coups : " + nb_essais;
    }

    function comparerImages(choix1, choix2) {
        if (choix1.getAttribute('src') === choix2.getAttribute('src')) {
            console.log("les images sont identiques")
            nb_paires_retournees++;
            if (nb_paires_retournees === nbImages) {
                document.getElementById('messageVictoire').innerHTML = "Bravo, vous avez gagné ! "
            }
        } else {
            console.log("les images ne sont pas identiques");
            setTimeout(() => {
                choix1.style.display = "none";
                choix2.style.display = "none";
                choix1.previousElementSibling.style.display = "block";
                choix2.previousElementSibling.style.display = "block";
            }, 1000);

        }
    }
}



//relancer le jeu = recharger la page

document.addEventListener('keydown', (e) => {
    if (e.code === "Space") {
        console.log("le jeu est relancé")
        window.location.reload();
    }
});


//Definir la collection et la taille

function genererSelecteurCollection() {

   var values = ["animaux", "animauxAnimes", "animauxdomestiques", "chiens", "dinosaures", "dinosauresAvecNom"];

    var select = document.createElement("select");
    select.name = "selecteurTheme";
    select.id = "selecteurTheme"

    for (const val of values) {
        var option = document.createElement("option");
        option.value = val;
        option.text = val.charAt(0).toUpperCase() + val.slice(1);
        select.appendChild(option);
    }

    var label = document.createElement("label");
    label.innerHTML = "Choisissez le thème :  "
    label.htmlFor = "selecteurTheme";

    document.getElementById("conteneurChoixTheme").appendChild(label).appendChild(select);
    document.getElementById('selecteurTheme').addEventListener("change", definirTheme);
}

function genererSelecteurTaille() {

    var values = [2,4,6,8,10,12];
    var select = document.createElement("select");
    select.name = "selecteurTaille";
    select.id = "selecteurTaille"

    for (const val of values) {
        var option = document.createElement("option");
        option.value = val;
        option.text = val;
        select.appendChild(option);
    }

    var label = document.createElement("label");
    label.innerHTML = "Choisissez le nombre de paires du jeu :  "
    label.htmlFor = "selecteurTaille";

    document.getElementById("conteneurChoixTaille").appendChild(label).appendChild(select);
    document.getElementById('selecteurTaille').addEventListener("change", definirTaille);
}




// //mélanger les images
// function shuffle(array) {
//     array.sort(() => Math.random() - 0.5);
//   }

//   let collectionImages=new Array(); 
//   collectionImages[0]= "ressources/animaux/1.webp"
//   collectionImages[1]= "ressources/animaux/2.webp"
//   collectionImages[2]= "ressources/animaux/3.webp"
//   collectionImages[3]= "ressources/animaux/4.webp"
//   collectionImages[4]= "ressources/animaux/1.webp"
//   collectionImages[5]= "ressources/animaux/2.webp"
//   collectionImages[6]= "ressources/animaux/3.webp"
//   collectionImages[7]= "ressources/animaux/4.webp"

//   shuffle(collectionImages);
// //console.log(collectionImages)

// let collectionMelangee=new Array(); 
// for (let i=0; i<8; i++){
//     collectionMelangee[i]=collectionImages[i];
// }

// // attribuer les images mélangées aux positions d'images d'origine
// for (let i=0; i<8; i++){
//     document.getElementById('carre_img_'+(i+1)).setAttribute('src',collectionMelangee[i]);
//     console.log(document.getElementById('carre_img_'+(i+1)));


// }

//generer un tableau
//initialiser le tableau d'index
