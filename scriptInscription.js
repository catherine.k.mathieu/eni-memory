window.onload = init;

function init() {
    document.getElementById('nomUtilisateur').addEventListener('input', verifierNom);
    document.getElementById('email').addEventListener('input', verifierMail);
    document.getElementById('mdp').addEventListener('input', verifierMDP);
    document.getElementById('mdp2').addEventListener('input', confirmerMDP);
    // document.getElementById('creationCompte').addEventListener('click', verifierInscription);
    

}
//-------------------------verification de la longueur du nom d'utilisateur-----------------------------------------------------------------------

function verifierNom(event) {
    let saisie = event.currentTarget.value
    let tailleOK = verifierTaille(saisie);

    if (tailleOK) {
        document.getElementById('textKONom').innerHTML = "";
        document.getElementById('messageKONom').style.display = "none";
        document.getElementById('messageOKNom').style.display = "inline-block";
    } else {
        document.getElementById('textKONom').innerHTML = "Choisissez un pseudo de minimum 3 caractères";
        document.getElementById('messageKONom').style.display = "inline-block";
        document.getElementById('messageOKNom').style.display = "none";
    }

    function verifierTaille(saisie) {
        return saisie.length >= 3;
    }
}
//-------------------------verification de la longueur du mail -------------------------------------------------------------------------------------------
function verifierMail(event) {
    let mail = event.currentTarget.value
    let mailOK = verifierRegexMail(mail);

    if (mailOK) {
        document.getElementById('textKOMail').innerHTML = "";
        document.getElementById('messageKOMail').style.display = "none";
        document.getElementById('messageOKMail').style.display = "inline-block";
    } else {
        document.getElementById('textKOMail').innerHTML = "Saisissez un email valide";
        document.getElementById('messageKOMail').style.display = "inline-block";
        document.getElementById('messageOKMail').style.display = "none";
    }

    function verifierRegexMail(mail) {
        let regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
        let mailOK = mail.match(regex);
        console.log(mailOK);
        return mailOK;
    }
}

//-------------------------verification du mot de passe  -------------------------------------------------------------------------------------------
function verifierMDP(event) {
    let saisie = event.currentTarget.value;

    let symboleOK = verifierSymbole(saisie);
    let chiffresOK = verifierChiffre(saisie);
    let tailleOK = verifierTaille(saisie);

    if (symboleOK && chiffresOK && tailleOK) {
        document.getElementById('messageKOMdp').style.display = "none";
        document.getElementById('messageOKMdp').style.display = "inline-block";
        console.log("les conditions sont remplies");
    } else {
        document.getElementById('messageKOMdp').style.display = "inline-block";
        document.getElementById('messageOKMdp').style.display = "none";
        console.log("les conditions ne sont pas remplies")
    }

    comparerMDP(document.getElementById('mdp2'), event.currentTarget);

    let securite = aiderSaisie(event.currentTarget.value);
    console.log(securite);
    switch (securite) {
        case vide :
            console.log("champ mdp vide");
            document.getElementById("mdpFaible").innerHTML = "";
            document.getElementById("mdpMoyen").innerHTML = "";
            document.getElementById("mdpFort").innerHTML = "";
            document.getElementById("mdpFaible").style.borderStyle = "hidden";
            document.getElementById("mdpMoyen").style.borderStyle = "hidden";
            document.getElementById("mdpFort").style.borderStyle = "hidden";
            break;
        case faible:
            console.log("faible");
            document.getElementById("mdpFaible").innerHTML = "Faible";
            document.getElementById("mdpFaible").style.borderStyle = "solid";
            document.getElementById("mdpMoyen").innerHTML = "";
            document.getElementById("mdpMoyen").style.borderStyle = "hidden";
            document.getElementById("mdpFort").innerHTML = "";
            document.getElementById("mdpFort").style.borderStyle = "hidden";
            break;
        case moyen:
            console.log("moyen");
            document.getElementById("mdpFaible").innerHTML = "Faible";
            document.getElementById("mdpFaible").style.borderStyle = "solid";
            document.getElementById("mdpMoyen").innerHTML = "Moyen";
            document.getElementById("mdpMoyen").style.borderStyle = "solid";
            document.getElementById("mdpFort").innerHTML = "";
            document.getElementById("mdpFort").style.borderStyle = "hidden";
            break;
        case fort:
            console.log("fort");
            document.getElementById("mdpFaible").innerHTML = "Faible";
            document.getElementById("mdpFaible").style.borderStyle = "solid";
            document.getElementById("mdpMoyen").innerHTML = "Moyen";
            document.getElementById("mdpMoyen").style.borderStyle = "solid";
            document.getElementById("mdpFort").innerHTML = "Fort";
            document.getElementById("mdpFort").style.borderStyle = "solid";
            break;
        default:
            console.log("oups");
            break;
    }
}


//-------------------------Confirmation du mot de passe-------------------------------------------------------------------------------------------
//CONFIRMATION MDP

function confirmerMDP(event) {
    comparerMDP(document.getElementById('mdp'), event.currentTarget);
}



//fonction contrôles des differentes contraintes utilisées dans verifier MDP
function verifierChiffre(saisie) {
    return saisie.match(/[0-9]/g) != null;
}

function verifierSymbole(saisie) {
    return saisie.match(/[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g) != null;
}

function verifierTaille(saisie) {
    return saisie.length >= 6;
}

//aide à la saisie
const vide = 0;
const faible = 1;
const moyen = 2;
const fort = 3;

function aiderSaisie(mdp) {
    let securite = 0;
    if (mdp.length === 0){
        securite = vide;
    }else if (mdp.length >= 9 && verifierSymbole(mdp) && verifierChiffre(mdp)) {
        securite = fort;
        console.log("le mot de passe est fort");

    } else if (mdp.length >= 6 && (verifierSymbole(mdp) || verifierChiffre(mdp))) {
        securite = moyen;
        console.log("le mot de passe est moyen");
    } else {
        securite = faible;
        console.log("le mot de passe est faible");
    }
    return securite;
}

function comparerMDP(input1, input2) {
    var mdpChoisi = input1.value;
    console.log(mdpChoisi);
    let confirmationMDP = input2.value;
    console.log(confirmationMDP);
    if (mdpChoisi===""||confirmationMDP===""){
        document.getElementById('KOconfirmMdp').style.display = "none";
        document.getElementById('OKconfirmMdp').style.display = "none";
    }else if (confirmationMDP == mdpChoisi) {
        document.getElementById('KOconfirmMdp').style.display = "none";
        document.getElementById('OKconfirmMdp').style.display = "inline-block";
        console.log("les mots de passe sont identiques");
    } else {
        document.getElementById('KOconfirmMdp').style.display = "inline-block";
        document.getElementById('OKconfirmMdp').style.display = "none";
        console.log("les mots ne sont pas identiques");
    }
}

//-------------------------Controle de tous les champs avant creation du compte-----------------------------------------------------------

// function verifierInscription() {

//     let nomOK = verifierNom(document.getElementById('nomUtilisateur').value);
//     let mailOK = verifierMail(document.getElementById('email').value);
//     let mdpOK = verifierMDP(document.getElementById('mdp').value);
//     let confirmMdpOK = confirmerMDP(document.getElementById('mdp2').value);

//     if (nomOK && mailOK && mdpOK && confirmMdpOK) {
//         console.log("passage dans verifier Inscription")
//     }
// }

