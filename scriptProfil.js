window.onload = init;


function init() {
    console.log("init");
    genererSelecteurCollection();
    genererSelecteurTaille();

}

function genererSelecteurCollection() {
console.log("passe par fonction selecteur collection");
    //var values = ["alphabet-scrabble", "animaux", "animauxAnimes", "animauxdomestiques", "chiens"];
    var values = ["animaux", "animauxAnimes", "animauxdomestiques", "chiens", "dinosaures", "dinosauresAvecNom"];
 
     var select = document.createElement("select");
     select.name = "selecteurTheme";
     select.id = "selecteurTheme"
 
     for (const val of values) {
         var option = document.createElement("option");
         option.value = val;
         option.text = val.charAt(0).toUpperCase() + val.slice(1);
         select.appendChild(option);
     }
 
     var label = document.createElement("label");
     label.innerHTML = "Choisissez le thème :  "
     label.htmlFor = "selecteurTheme";
 
     document.getElementById("conteneurChoixTheme").appendChild(label).appendChild(select);
    // document.getElementById('selecteurTheme').addEventListener("change", definirTheme);
 }
 
 function genererSelecteurTaille() {
    console.log("passe par fonction selecteur collection");
 
     var values = [2,4,6,8,10,12];
     //var values = [];
     //var selectionTheme = document.getElementById('selecteurTheme').value;
     //var tailleMax = definirTailleMax(selectionTheme);
   
     //for (let i=tailleMinCollection;i<=tailleMax;i++) {
       //  values.push(2*i);
     //}
 
     var select = document.createElement("select");
     select.name = "selecteurTaille";
     select.id = "selecteurTaille"
 
     for (const val of values) {
         var option = document.createElement("option");
         option.value = val;
         option.text = val;
         select.appendChild(option);
     }
 
     var label = document.createElement("label");
     label.innerHTML = "Choisissez le nombre de cartes du jeu :  "
     label.htmlFor = "selecteurTaille";
 
     document.getElementById("conteneurChoixTaille").appendChild(label).appendChild(select);
     document.getElementById('selecteurTaille').addEventListener("change", definirTaille);
 }